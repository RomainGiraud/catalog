<!-- PROJECT LOGO -->
<br />
<p align="center">
  <h3 align="center">catalog</h3>

  <p align="center">
    Manage a movie database
    <br />
    <a href="https://gitlab.com/RomainGiraud/catalog"><strong>Explore the docs »</strong></a>
    <br />
    <a href="https://gitlab.com/RomainGiraud/catalog/-/issues">Report Bug or Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)



<!-- ABOUT THE PROJECT -->
## About The Project

I made this tool to help me visualize package dependencies. I hope it cans help you too!


### Built With

* [sqlite3](https://sqlite.org/)



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

* C++ build environment

### Installation

Clone the repo
```sh
git clone https://gitlab.com/RomainGiraud/catalog.git
```


<!-- USAGE EXAMPLES -->
## Usage

This tool use a server / client model.
It only provides the server, and you can use any network client (telnet, netcat...).

You must start the server:
```sh
./serveur 8080
```

### Commands

All commands must finished with "FIN".

* LIST: list movies in the database
* SHOW <title>: display details of a movie in the database
* SEARCH <title>: search for a movie on Internet
* ADD <title> <index>: add a movie with the specified title and the index corresponding to the SEARCH command
* REMOVE <title> <index>: remove a movie

![](images/example.gif)

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Romain Giraud - dev@romaingiraud.com
Kévin Vicrey

Project Link: [https://gitlab.com/RomainGiraud/catalog](https://gitlab.com/RomainGiraud/catalog)
