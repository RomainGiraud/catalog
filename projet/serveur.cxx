#include "CException.h"
#include "CDatabase.h"
#include "constants.h"
//#include "allocine_fr.h"
#include "stub.h"

#include <sys/types.h>  // socket(), bind()
#include <sys/socket.h> // socket(), bind()
#include <arpa/inet.h>  // sockaddr_in
#include <signal.h>     // signal()
#include <sys/wait.h>   // wait()
#include <string.h>     // memset()
#include <stdlib.h>     // atoi()
#include <limits.h>     // MAX_INPUT

#include <iostream>
#include <sstream>
#include <string>

using namespace std;
using namespace nsCatalog;

namespace
{
    void SignalFils (int Num)
    {
        wait (0);
    }

    void Write (int sd, char *buf, size_t taille)
    {
        strcat (buf, "\nFIN");
        if (write (sd, buf, taille) < 0)
            throw CException (ERR_WRITE);

    }

    void Write (int sd, string Msg)
    {
        Msg += "\nFIN";
        if (write (sd, Msg.c_str(), Msg.size()) < 0)
            throw CException (ERR_WRITE);

    }

    bool hasEnding (std::string const &fullString, std::string const &ending)
    {
        if (fullString.length() < ending.length())
            return false;

        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));

    }

    string CmdList()
    {
        CDatabase db;

        try {
            db.Init();
        } catch (const CException &e) {
            throw CException (ERR_CONN_DB);
        }

        VFilm_t VFilm;
        try {
            VFilm = db.GetAllFilms();
        } catch (const CException &e) {
            throw CException (ERR_DB);
        }

        ostringstream oss;
        oss << SUCCESS << '\n';
        for (VFilm_t::iterator i = VFilm.begin(); i < VFilm.end(); i++)
            oss << (*i).GetId() << ' ' << (*i).GetTitre() << '\n';

        return oss.str();

    }

    string CmdSearch (string Titre)
    {
        stub Net;
        VFilm_t VFilm;
        
        try {
            VFilm = Net.Recherche (Titre);
        } catch (const CException &e) {
            throw CException (ERR_NOT_FND);
        }

        ostringstream oss;
        oss << SUCCESS << '\n';
        unsigned Num (0);
        for (VFilm_t::iterator i = VFilm.begin(); i < VFilm.end(); i++)
            oss << ++Num << ' ' << (*i).GetTitre() << '\n';

        return oss.str();

    }

    string CmdImage (int sd, string Titre)
    {
        CDatabase db;

        try {
            db.Init();
        } catch (const CException &e) {
            throw CException (ERR_CONN_DB);
        }

        VFilm_t VFilm;
        try {
            VFilm = db.GetFilm (Titre);
        } catch (const CException &e) {
            throw CException (ERR_NOT_FND);
        }

        ostringstream succ;
        succ << SUCCESS << "\n";

        ostringstream oss;
        oss << VFilm.at(0).GetIdImg();
        ifstream ifs (oss.str().c_str());
        char Buf[MAX_INPUT];
        while (1)
        {
            int NbLu = ifs.readsome (Buf, MAX_INPUT);
            if (write (sd, Buf, NbLu) <= 0)
                break;
        }

        if (write (sd, "FIN", 3) <= 0)
            throw CException (ERR_WRITE);

        return "";

    }

    string CmdShow (string Titre)
    {
        CDatabase db;

        try {
            db.Init();
        } catch (const CException &e) {
            throw CException (ERR_CONN_DB);
        }

        VFilm_t VFilm;
        try {
            VFilm = db.GetFilm (Titre);
        } catch (const CException &e) {
            throw CException (ERR_NOT_FND);
        }

        ostringstream oss;
        oss << SUCCESS << '\n';
        if (VFilm.size() > 1)
            for (VFilm_t::iterator i = VFilm.begin(); i < VFilm.end(); i++)
                oss << (*i).GetId() << ' ' << (*i).GetTitre() << '\n';
        else
            oss << VFilm[0].GetId()    << '\n'
                << VFilm[0].GetTitre() << '\n'
                << VFilm[0].GetDesc()  << '\n'
                << VFilm[0].GetGenre() << '\n'
                << VFilm[0].GetRealisateur() << '\n'
                << VFilm[0].GetActeurs()  << '\n'
                << VFilm[0].GetJour() << '/' << VFilm[0].GetMois() << '/' << VFilm[0].GetAnnee() << '\n'
                << VFilm[0].GetDuree() << '\n';

        return oss.str();

    }

    string CmdAdd (string Titre, unsigned Num)
    {
        if (!Num--)
            throw CException (ERR_CMD);

        stub Net;
        VFilm_t VFilm;
        try {
            VFilm = Net.Recherche (Titre);
        } catch (const CException & e) {
            throw CException (ERR_NOT_FND);
        }

        CDatabase db;
        try {
            db.Init();
        } catch (const CException &e) {
            throw CException (ERR_CONN_DB);
        }

        try
        {
            bool Exists (false);
            try {
                db.GetFilm (VFilm.at(Num).GetTitre());
                Exists = true;
            } catch (const CException &) { }

            if (Exists) throw CException (ERR_RDY_ADD);

            Net.Detail (VFilm.at(Num));
            db.Ajouter (VFilm.at(Num));
        }
        catch (const exception & e) {
            throw CException (ERR_INDICE);
        }

        ostringstream oss;
        oss << SUCCESS << '\n';
        return oss.str();

    }

    string CmdRemove (string Titre, int Num)
    {
        if (!Num--)
            throw CException (ERR_CMD);

        CDatabase db;
        try {
            db.Init();
        } catch (const CException &e) {
            throw CException (ERR_CONN_DB);
        }

        VFilm_t VFilm;
        try {
            VFilm = db.GetFilm (Titre);
        } catch (const CException &e) {
            throw CException (ERR_NOT_FND);
        }

        unsigned img = VFilm.at(Num).GetIdImg();

        try {
            db.Supprimer (VFilm.at(Num));
        } catch (const exception & e) {
            throw CException (ERR_INDICE);
        }

        ostringstream strImg;
        strImg << img;
        unlink (strImg.str().c_str());

        ostringstream oss;
        oss << SUCCESS << '\n';
        return oss.str();

    }

}

int main (int argc, char **argv)
{
    int sd (0);

    signal (SIGCHLD, SignalFils);

    try
    {
        if (argc == 1)
            throw CException ("Usage: serveur <port>");
        if ((sd = socket (PF_INET, SOCK_STREAM, 0)) < 0)
            throw CException ("Impossible de créer la socket.");

        sockaddr_in Addr;
        memset (&Addr, 0, sizeof (Addr));
        Addr.sin_family = AF_INET;
        Addr.sin_port = htons (atoi (argv[1]));
        Addr.sin_addr.s_addr = INADDR_ANY;

        if (bind (sd, (sockaddr *) & Addr, sizeof (Addr)) < 0)
            throw CException ("Impossible d'attacher la socket (bind).");

        if (listen (sd, 32) < 0)
            throw CException ("Impossible d'écouter sur la socket (listen).");

        while (true)
        {
            int sdCom (accept (sd, 0, 0)), NbLu (0);

            if (fork()) continue;

            close (sd);

            try
            {
                while (true)
                {
                    string Choix;
                    while (true)
                    {
                        char Msg[MAX_INPUT];
                        memset(Msg, '\0', MAX_INPUT);
                        if ((NbLu = read (sdCom, (void *) & Msg, MAX_INPUT)) <= 0)
                            throw CException ("Erreur de lecture."); 

                        while (NbLu >= 0 && (isspace (Msg[NbLu]) || Msg[NbLu] == '\0'))
                        {
                                Msg[NbLu] = '\0';
                                --NbLu;
                        }

                        if (NbLu <= 0)
                            continue;

                        Choix += string (Msg);

                        if (hasEnding(Choix, "FIN"))
                            break;

                        Choix += " ";
                    }

                    Choix = Choix.substr(0, Choix.size()-4);

                    cout << Choix << endl;
                    string Retour;

                    try
                    {
                        if (Choix == "LIST")
                            Retour = CmdList();
                        else if (!Choix.find ("IMAGE"))
                        {
                            CmdImage (sdCom, Choix.substr (6, Choix.size()));
                            continue;
                        }
                        else if (!Choix.find ("SHOW"))
                            Retour = CmdShow (Choix.substr (5, Choix.size()));
                        else if (!Choix.find ("SEARCH"))
                            Retour = CmdSearch(Choix.substr (7, Choix.size()));
                        else if (!Choix.find ("ADD"))
                            Retour = CmdAdd (
                                Choix.substr (4, Choix.find_last_of (' ')-4), 
                                atoi (Choix.substr (Choix.find_last_of (' '), 
                                              Choix.size()).c_str())
                            );
                        else if (!Choix.find ("REMOVE"))
                            Retour = CmdRemove (
                                Choix.substr (7, Choix.find_last_of (' ')-7), 
                                atoi (Choix.substr (Choix.find_last_of (' '), 
                                              Choix.size()).c_str())
                            );
                        else
                            throw CException (ERR_CMD);

                        Write (sdCom, Retour);

                    }
                    catch (const CException &e) 
                    {
                        try {
                            Write (sdCom, e.GetStr() + "\n");
                        }
                        catch (...) { }
                    }
                    catch (const exception &e)
                    {
                        ostringstream oss;
                        oss << ERR_CMD << '\n';

                        try {
                            Write (sdCom, oss.str());
                        }
                        catch (...) { }
                    }
                }
            }
            catch (const CException &e) { }

            close (sdCom);
            return EXIT_SUCCESS;
        }
    }
    catch (const CException & e)
    {
        cerr << e.GetStr() << endl;
    }

    if (sd >= 0)
        close (sd);

    return EXIT_SUCCESS;
}
