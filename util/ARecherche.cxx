#include "ARecherche.h"

#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace nsCatalog;

vector<string> ARecherche::Extraire (const string & Texte, string Begin, string End) const
{
    int Pos (0), Taille (Begin.size());
    vector<string> VResult;

    while (true)
    {
        if ((Pos = Texte.find (Begin, Pos+1)) == -1)
            break;
        Pos += Taille;

        try
        {
            int PosEnd = Texte.find (End, Pos);

            string Temp (Texte.substr (Pos, PosEnd - Pos));
            VResult.push_back (Temp);

            Pos = PosEnd + End.size();
        }
        catch (const exception &) { break; }
    }

    return VResult;

}
