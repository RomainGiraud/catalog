#include "CDatabase.h"
#include <stdlib.h>     // atoi()

#include <iostream>
#include <sstream>

using namespace std;
using namespace nsCatalog;

VFilm_t CDatabase::s_films;

void CDatabase::Init()
{
    if (sqlite3_open (m_dbName.c_str(), &m_Db))
        throw CException (sqlite3_errmsg(m_Db));

    Exec("CREATE TABLE IF NOT EXISTS films ("
         "  id INTEGER PRIMARY KEY AUTOINCREMENT,"
         "  titre TEXT NOT NULL,"
         "  description TEXT NOT NULL,"
         "  genre TEXT NOT NULL,"
         "  realisateur TEXT NOT NULL,"
         "  acteurs TEXT NOT NULL,"
         "  jour_sortie INTEGER NOT NULL,"
         "  mois_sortie INTEGER NOT NULL,"
         "  annee_sortie INTEGER NOT NULL,"
         "  duree INTEGER NOT NULL,"
         "  idimg INTEGER)");
}

void CDatabase::Delete() const
{
    Exec("DROP TABLE films");
}

void CDatabase::Exec (string query) const
{
    char *errMsg = 0;
    if (sqlite3_exec(m_Db, query.c_str(), CDatabase::recupFilm, 0, &errMsg)
            != SQLITE_OK)
    {
        string str (errMsg);
        sqlite3_free(errMsg);
        throw CException (str);
    }
    sqlite3_free(errMsg);
}

int CDatabase::recupFilm(void *NotUsed, int argc, char **argv, char **azColName)
{
    CFilm f;
    f.SetId(atoi(argv[0]));
    f.SetTitre(argv[1]);
    f.SetDesc(argv[2]);
    f.SetGenre(argv[3]);
    f.SetRealisateur(argv[4]);
    f.SetActeurs(argv[5]);

    f.SetJour(atoi(argv[6]));
    f.SetMois(atoi(argv[7]));
    f.SetAnnee(atoi(argv[8]));
    f.SetDuree(atoi(argv[9]));
    f.SetIdImg(atoi(argv[10]));

    s_films.push_back(f);
    return 0;
}

VFilm_t CDatabase::GetFilm(string titre) const
{
    s_films.erase(s_films.begin(),s_films.end());
    Exec("SELECT * FROM films WHERE titre LIKE \""+titre+"\"");

    if (!s_films.size())
        throw CException ("Aucun film trouvé.");

    return s_films;
}

VFilm_t CDatabase::GetAllFilms() const
{
    s_films.erase(s_films.begin(),s_films.end());
    Exec("SELECT * FROM films");
    return s_films;
}

inline string CDatabase::Escape (string Str) const
{
    istringstream iss (Str);
    ostringstream oss;

    string Temp;
    while (getline (iss, Temp, '"'))
        oss << Temp << "\"\"";

    return oss.str().substr (0, oss.str().size()-2);
}

void CDatabase::Ajouter (const CFilm & Film) const
{
    bool Exists (false);
    try {
        GetFilm(Film.GetTitre());
        Exists = true;
    } catch (const CException &) { }

    if (Exists) throw CException ("Le film est déjà enregistré.");

    stringstream jour, mois, annee, duree, idImg;
    jour  << dec << Film.GetJour();
    mois  << Film.GetMois();
    annee << Film.GetAnnee();
    duree << Film.GetDuree();
    idImg << Film.GetIdImg();

    Exec("INSERT INTO films VALUES ("
         "NULL,"
         "\"" + Escape (Film.GetTitre()) + "\","
         "\"" + Escape (Film.GetDesc()) + "\","
         "\"" + Escape (Film.GetGenre()) + "\","
         "\"" + Escape (Film.GetRealisateur()) + "\","
         "\"" + Escape (Film.GetActeurs()) + "\","
         "" + jour.str() + ","
         "" + mois.str() + ","
         "" + annee.str() + ","
         "" + duree.str() + ","
         "" + idImg.str() + ")");
}

void CDatabase::Modifier (const CFilm & Film) const
{
    stringstream id, jour, mois, annee, duree;
    id    << Film.GetId();
    jour  << Film.GetJour();
    mois  << Film.GetMois();
    annee << Film.GetAnnee();
    duree << Film.GetDuree();

    Exec("UPDATE films SET "
         "titre=\"" + Film.GetTitre() + "\","
         "description=\"" + Film.GetDesc() + "\","
         "genre=\"" + Film.GetGenre() + "\","
         "realisateur=\"" + Film.GetRealisateur() + "\","
         "acteurs=\"" + Film.GetActeurs() + "\","
         "jour_sortie=" + jour.str() + ","
         "mois_sortie=" + mois.str() + ","
         "annee_sortie=" + annee.str() + ","
         "duree=" + duree.str() + " "
         "WHERE id="+id.str());
}

void CDatabase::Supprimer(const CFilm & Film) const
{
    stringstream id;
    id << Film.GetId();

    Exec("DELETE FROM films WHERE id="+id.str());
}
