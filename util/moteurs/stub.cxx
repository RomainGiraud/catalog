#include <map>
#include <string.h>
#include <iconv.h>
#include <stdlib.h>     // atoi()
#include <exception>
#include <string>
#include <iostream>
#include <sstream>
#include <errno.h>

#include "util.h"
#include "stub.h"
#include "CFilm.h"

using namespace std;
using namespace nsCatalog;

stub::stub()
    : ARecherche ("STUB")
{ }

stub::~stub() { }

string stub::GetPage (string Url)
{
    return string ();
}

VFilm_t stub::Recherche (string MotCle)
{
    if (MotCle == "Interstellar")
    {
        CFilm F;
        F.SetTitre ("Interstellar");
        F.SetUrl ("film/fichefilm_gen_cfilm");
        F.SetAnnee (2014);

        VFilm_t VFilms;
        VFilms.push_back (F);
        return VFilms;
    }

    throw CException ("Aucun film trouvé sur internet.");
}

CFilm & stub::Detail (CFilm & Film)
{
    if (Film.GetTitre() == "Interstellar")
    {
        Film.SetJour  (5);
        Film.SetMois  (11);
        Film.SetAnnee (2014);
        Film.SetRealisateur ("Christopher Nolan");
        Film.SetDesc ("Le film raconte les aventures d’un groupe d’explorateurs qui utilisent une faille récemment découverte dans l’espace-temps afin de repousser les limites humaines et partir à la conquête des distances astronomiques dans un voyage interstellaire.");
        Film.SetActeurs ("Matthew McConaughey, Anne Hathaway, Michael Caine");
        Film.SetGenre ("Science fiction");
        Film.SetDuree (169);
    }

    return Film;
}
