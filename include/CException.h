/**
 * @File   : CException.h
 *
 * @Author : Kévin Vicrey, Romain Deveaud
 *
 * @Date   : 11/12/2007
 *
**/

#if !defined __CEXCEPTION_H__
#define      __CEXCEPTION_H__

#include <string>

namespace nsCatalog
{
    class CException
    {
      public:
        CException (std::string Str = "") noexcept(false);
        CException (int Num)              noexcept(false);
        ~CException (void)                noexcept(false);

        const std::string & GetStr (void) const noexcept(false);

      private:
        std::string m_Str;

    };

    class CExceptCar : public CException { };
}

#include "CException.hxx"

#endif    /*  __CEXCEPTION_H__ */
