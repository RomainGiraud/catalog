#if !defined __STUB_H__
#define      __STUB_H__

#include <vector>
#include <string>

#include "ARecherche.h"
#include "CFilm.h"

namespace nsCatalog
{
    class stub : public ARecherche
    {
      public:
        stub();
        ~stub();

        VFilm_t Recherche (std::string MotCle);
        CFilm & Detail    (CFilm & Film      );

        std::string GetPage (std::string Url);
    };
}


#endif
