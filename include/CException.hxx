/**
 * @File   : CException.hxx
 *
 * @Author : Kévin Vicrey
 *
 * @Date   : 11/12/2007
 *
**/

#if !defined __CEXCEPTION_HXX__
#define      __CEXCEPTION_HXX__

#include <sstream>

#define CEXCEPTION nsCatalog::CException

inline CEXCEPTION::CException (int Num) noexcept(false)
{
    std::ostringstream oss;
    oss << Num;
    m_Str = oss.str();
}

inline CEXCEPTION::CException (std::string Str) noexcept(false)
    : m_Str (Str) { }

inline CEXCEPTION::~CException (void) noexcept(false) {}

inline const std::string & CEXCEPTION::GetStr (void) const noexcept(false)
{
    return m_Str;
}

#endif    /*  __CEXCEPTION_HXX__ */

