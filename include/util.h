#if !defined __UTIL_H__
#define      __UTIL_H__

#include <string>

namespace nsCatalog
{    
    void Urlencode (std::string & Str);
}

#endif
