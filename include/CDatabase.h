#if !defined __CDATABASE_H__
#define      __CDATABASE_H__

#include <string>
#include <sstream>

#include "CException.h"
#include "CFilm.h"
#include "constants.h"
#include "sqlite3.h"

namespace nsCatalog
{
    class CDatabase
    {
      public:
        CDatabase(const std::string &db = CstDbFileName);
        ~CDatabase();

        void Init();
        void Delete() const;

        void Ajouter  (const CFilm & Film) const;
        void Modifier (const CFilm & Film) const;
        void Supprimer(const CFilm & Film) const;

        VFilm_t GetFilm(std::string titre) const;
        VFilm_t GetAllFilms()              const;

      private:
        const std::string m_dbName;
        sqlite3 * m_Db;
        static VFilm_t s_films;

        void Exec (std::string query) const;
        static int recupFilm(void *NotUsed, int argc, char **argv, char **azColName);

        std::string Escape (std::string Str) const;
    };
}

#include "CDatabase.hxx"

#endif
