# Les fichiers inclus
CONSTANTS_H  = include/constants.h
CEXCEPTION_H = include/CException.h include/CException.hxx
CCONNEXION_H = include/CConnexion.h include/CConnexion.hxx $(CEXCEPTION_H)
CFILM_H      = include/CFilm.h include/CFilm.hxx 
UTIL_H       = include/util.h
CDATABASE_H  = include/CDatabase.h include/CDatabase.hxx \
               $(CEXCEPTION_H) $(CFILM_H)

# moteurs de recherche
ARECHERCHE_H = include/ARecherche.h include/ARecherche.hxx \
			   $(CFILM_H) $(CCONNEXION_H)
ALLOCINE_FR_H = $(ARECHERCHE_H) $(CFILM_H) $(UTIL_H)
STUB_H 		  = $(ARECHERCHE_H) $(CFILM_H) $(UTIL_H)

# Commande générique pour compiler
OPT = -Wall -O3 -Iinclude/ -Iinclude/moteurs/
COMPILER = g++ -c $< -o $*.o $(OPT)

# Nom du programme à compiler
nom = serveur

$(nom): lib/$(nom).o lib/CConnexion.o lib/CFilm.o lib/CDatabase.o \
		lib/allocine_fr.o lib/stub.o lib/sqlite3.o lib/ARecherche.o \
		lib/util.o
	g++ -s $^ -o $(nom) $(OPT) -lpthread -ldl

lib/$(nom).o: projet/$(nom).cxx $(CEXCEPTION_H) $(CONSTANTS_H)
	$(COMPILER)

lib/CConnexion.o: util/CConnexion.cxx $(CCONNEXION_H)
	$(COMPILER)

lib/util.o: util/util.cxx $(UTIL_H)
	$(COMPILER)

lib/CFilm.o: util/CFilm.cxx $(CFILM_H)
	$(COMPILER)

lib/CDatabase.o : util/CDatabase.cxx $(CDATABASE_H)
	$(COMPILER)

lib/ARecherche.o : util/ARecherche.cxx $(ARECHERCHE_H)
	$(COMPILER)

lib/allocine_fr.o : util/moteurs/allocine_fr.cxx $(ALLOCINE_FR_H)
	$(COMPILER)

lib/stub.o : util/moteurs/stub.cxx $(STUB_H)
	$(COMPILER)

lib/sqlite3.o : util/sqlite3.c include/sqlite3.h
	gcc -c $< -o $*.o -w

clean:
	rm lib/*.o -fv; rm serveur -fv;
